from unittest import TestCase
from unittest.mock import patch

from requests.exceptions import Timeout, ConnectionError
import responses

from address_checker.exceptions import SwissPostNetworkError, SwissPostServerError
from address_checker.swisspost import SwisspostAddressChecker


class SwisspostAddressCheckerTestCase(TestCase):

    def test_init_no_username(self):
        connector = SwisspostAddressChecker()

        self.assertEqual(connector.auth, None)

    def test_init_with_proxy(self):
        connector = SwisspostAddressChecker(
            username='test',
            password='test',
            proxy_url='testproxy',
            proxy_port='testproxyport',
        )

        self.assertEqual(
            connector.proxies['http'],
            'http://testproxy:testproxyport'
        )

        self.assertEqual(
            connector.proxies['https'],
            'https://testproxy:testproxyport'
        )

    @patch('address_checker.swisspost.requests.get', side_effect=Timeout())
    def test_get_intercept_timeout_errors(self, m_get):
        connector = SwisspostAddressChecker(username='test', password='test')
        with self.assertRaises(SwissPostNetworkError):
            connector._get('ping')

    @patch('address_checker.swisspost.requests.get', side_effect=ConnectionError())
    def test_get_intercept_connection_errors(self, m_post):
        connector = SwisspostAddressChecker(username='test', password='test')
        with self.assertRaises(SwissPostNetworkError):
            connector._get('ping')

    @patch('address_checker.swisspost.requests.post', side_effect=Timeout())
    def test_post_intercept_timeout_errors(self, m_get):
        connector = SwisspostAddressChecker(username='test', password='test')
        with self.assertRaises(SwissPostNetworkError):
            connector._post('ping', {})

    @patch('address_checker.swisspost.requests.post', side_effect=ConnectionError())
    def test_post_intercept_connection_errors(self, m_post):
        connector = SwisspostAddressChecker(username='test', password='test')
        with self.assertRaises(SwissPostNetworkError):
            connector._post('ping', {})

    @responses.activate
    def test_get_intercepts_500_errors(self):
        connector = SwisspostAddressChecker(username='test', password='test')
        responses.add(
            responses.GET,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/ping',
            json={},
            status=500
        )
        with self.assertRaises(SwissPostServerError):
            connector._get('ping')

    @responses.activate
    def test_post_intercepts_500_errors(self):
        connector = SwisspostAddressChecker(username='test', password='test')
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/ping',
            json={},
            status=500
        )
        with self.assertRaises(SwissPostServerError):
            connector._post('ping', {})
