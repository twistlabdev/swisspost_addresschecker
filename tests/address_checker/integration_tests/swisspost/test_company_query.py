import json
from unittest import TestCase

import responses

from address_checker.exceptions import InvalidGeoCoordinateFormat
from address_checker.swisspost import SwisspostAddressChecker
from tests.fixtures.swiss_responses import query_response


class SwisspostAddressCheckerCompanyQueryTestCase(TestCase):

    @responses.activate
    def test_query_ok(self):
        response_body = query_response()
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.company_address_query(
            'Julien',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel'
        )

        self.assertDictEqual(
            response.result,
            {
                'Data': {
                    'CustomID_01_in': '',
                    'QSTAT': {
                        'Code': '1',
                        'Definition': 'Person hit',
                        'Description': 'This person (first names and last name) is known at this address and items can be delivered.'
                    },
                    'ShortReport': '-11111111111414411111A-A',
                    'CorrectionType': {'Code': '0',
                             'Definition': 'No correction required'},
                    'TotalScore': '100.00',
                    'ValidFrom': '',
                    'ValidTo': '',
                    'Birthdate': '',
                    'HouseKey': '76216853',
                    'Double_DoubleGroupNo': '',
                    'Double_DSTAT': '',
                    'Company': '',
                    'Prename': 'Larissa',
                    'Prename2': '',
                    'Name': 'Harnisch',
                    'MaidenName': '',
                    'AddressAddition': '',
                    'CoAddress': '',
                    'StreetName': 'Bürenstrasse',
                    'HouseNo': '45',
                    'HouseNoAddition': '',
                    'Floor': '',
                    'ZIPCode': '2504',
                    'ZIPAddition': '00',
                    'TownName': 'Biel/Bienne',
                    'Canton': 'BE',
                    'CountryCode': 'CH',
                    'POBoxTerm': '',
                    'POBoxNo': '',
                    'POBoxZIP': '',
                    'POBoxZIPAddition': '',
                    'POBoxTownName': ''
                },
                'Id': 312312312
            }
        )

    @responses.activate
    def test_query_invalid_geo_coordinate(self):
        response_body = query_response(status=802)
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        with self.assertRaises(InvalidGeoCoordinateFormat) as exc:
            connector.company_address_query(
                'Julien',
                'Strickstrasse',
                '5',
                '8174',
                'Stadel'
            )

            self.assertEqual(exc.response_data, response_body)

    @responses.activate
    def test_result_hit_personal_hit(self):
        response_body = query_response()
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.company_address_query(
            'Julien',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel'
        )

        self.assertEqual(response.qstat.code, '1')
        self.assertEqual(response.qstat.designation, 'Person hit')
        self.assertEqual(
            response.qstat.description,
            'This person (first names and last name) is known at this address and items can be delivered.'
        )

    @responses.activate
    def test_result_hit_address_unknown(self):
        response_body = query_response()
        response_body['RunQuery2Result']['Result']['Data'][1]['Value'] = '51'
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.individual_address_query(
            'Julien',
            'Nyffeler',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel'
        )

        self.assertEqual(response.qstat.code, '51')
        self.assertEqual(response.qstat.designation, 'Address unknown')
        self.assertEqual(
            response.qstat.description,
            'This address is incorrect or incomplete.'
        )
    
    @responses.activate
    def test_result_hit_no_response(self):
        response_body = query_response()
        response_body['RunQuery2Result']['Result']['Data'][1]['Value'] = ''
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.company_address_query(
            'Julien',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel'
        )

        self.assertEqual(response.qstat.code, '-1')
        self.assertEqual(response.qstat.designation, 'Non-deliverable')
        self.assertEqual(
            response.qstat.description,
            'Non-deliverable or Delivery not guaranteed.'
        )


    @responses.activate
    def test_correct_type_no_correction_required(self):
        response_body = query_response()
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.company_address_query(
            'Julien',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel'
        )

        self.assertEqual(response.correct_type_detail, 'No correction required')

    @responses.activate
    def test_correct_type_correction_required(self):
        response_body = query_response()
        response_body['RunQuery2Result']['Result']['Data'][3]['Value'] = '1'
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )

        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.individual_address_query(
            'Julien',
            'Nyffeler',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel'
        )

        self.assertEqual(response.correct_type_detail, 'Correction required')

    @responses.activate
    def test_with_external_id(self):
        response_body = query_response()
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/runquery2',
            json=response_body,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')
        connector.company_address_query(
            'Julien',
            'Strickstrasse',
            '5',
            '8174',
            'Stadel',
            external_id='testid'
        )

        self.assertEqual(len(responses.calls), 1)
        self.assertEqual(
            json.loads(responses.calls[0].request.body)['address']['Id'],
            'testid'
        )