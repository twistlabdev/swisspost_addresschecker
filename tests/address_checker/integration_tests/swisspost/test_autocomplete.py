from unittest import TestCase

import responses

from address_checker.exceptions import DatabaseItemNotFound
from address_checker.swisspost import SwisspostAddressChecker
from tests.fixtures.swiss_responses import autocomplete_response


class SwisspostAddressCheckerAutocompleteTestCase(TestCase):

    @responses.activate
    def test_autocomplete(self):
        response_data = autocomplete_response()
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/autocomplete2',
            json=response_data,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.autocomplete(
            'Linden',
            '1',
            '60',
            'Stadel'
        )

        self.assertEqual(response.result, response_data['QueryAutoComplete2Result']['AutoCompleteResult'])

    @responses.activate
    def test_autocomplete_invalid_geo_coordinate(self):
        response_data = autocomplete_response(status=302)
        responses.add(
            responses.POST,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/autocomplete2',
            json=response_data,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')
        
        with self.assertRaises(DatabaseItemNotFound) as exc:
            response = connector.autocomplete(
                'Linden',
                '1',
                '60',
                'Stadel'
            )

            self.assertEqual(exc.response_data, response_data)
