from unittest import TestCase

import responses

from address_checker.swisspost import SwisspostAddressChecker
from tests.fixtures.swiss_responses import verification_response


class SwisspostAddressCheckerBuildingVerificationTestCase(TestCase):

    @responses.activate
    def test_build_verification(self):
        response_data = verification_response()
        query_params = '?streetname=Viktoriastrasse&houseNo=21&houseNoAddition=33&zipcode=3013&townname=Berne'
        url = 'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/simplequery/buildingverification{}'.format(
            query_params
        )
        responses.add(
            responses.GET,
            url,
            json=response_data,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.building_verification(
            'Viktoriastrasse',
            '21',
            '33',
            '3013',
            'Berne',
        )

        self.assertEqual(
            response.result,
            {
                "HouseKey": 7013859,
                "HouseNumber": "21",
                "HouseNumberAddition": "",
                "HouseNumberHouseNumberAddition": None,
                "Pstat": {
                    'Code': response.p_stat.code,
                    'Definition': response.p_stat.designation,
                    'Description': response.p_stat.description
                },
                "Streetname": "Viktoriastrasse",
                "StreetnameLong": "Viktoriastrasse",
                "TownName": "Bern",
                "TownNameLong": "Bern",
                "ZipAddition": "00",
                "Zipcode": "3013"
            }
        )

    @responses.activate
    def test_pstat(self):
        response_data = verification_response()
        query_params = '?streetname=Viktoriastrasse&houseNo=21&houseNoAddition=33&zipcode=3013&townname=Berne'
        url = 'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/simplequery/buildingverification{}'.format(
            query_params
        )
        responses.add(
            responses.GET,
            url,
            json=response_data,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.building_verification(
            'Viktoriastrasse',
            '21',
            '33',
            '3013',
            'Berne',
        )

        self.assertEqual(response.p_stat.code, 4)
        self.assertEqual(response.p_stat.designation, 'supplemented')
        self.assertEqual(response.p_stat.description, 'Information missing from the address can be supplemented.')
        self.assertEqual(
            str(response.p_stat),
            '4 - supplemented: Information missing from the address can be supplemented.'
        )
        self.assertEqual(
            response.p_stat.result_report,
            [4, 'supplemented', 'Information missing from the address can be supplemented.']
        )

    @responses.activate
    def test_pstat_default_response(self):
        response_data = verification_response()
        response_data['QueryBuildingVerificationResult']['BuildingVerificationData']['Pstat'] = ''
        query_params = '?streetname=Viktoriastrasse&houseNo=21&houseNoAddition=33&zipcode=3013&townname=Berne'
        url = 'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/simplequery/buildingverification{}'.format(
            query_params
        )
        responses.add(
            responses.GET,
            url,
            json=response_data,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.building_verification(
            'Viktoriastrasse',
            '21',
            '33',
            '3013',
            'Berne',
        )

        self.assertEqual(response.p_stat.code, '-1')
        self.assertEqual(response.p_stat.designation, 'No results')
        self.assertEqual(response.p_stat.description, 'No results returned by SwissPost API.')

    @responses.activate
    def test_pstat_unknown_code(self):
        response_data = verification_response()
        response_data['QueryBuildingVerificationResult']['BuildingVerificationData']['Pstat'] = '3333'
        query_params = '?streetname=Viktoriastrasse&houseNo=21&houseNoAddition=33&zipcode=3013&townname=Berne'
        url = 'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/simplequery/buildingverification{}'.format(
            query_params
        )
        responses.add(
            responses.GET,
            url,
            json=response_data,
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')

        response = connector.building_verification(
            'Viktoriastrasse',
            '21',
            '33',
            '3013',
            'Berne',
        )

        self.assertEqual(response.p_stat.code, '3333')
        self.assertEqual(response.p_stat.designation, 'No results')
        self.assertEqual(response.p_stat.description, 'No results returned by SwissPost API.')