from unittest import TestCase

import responses

from address_checker.swisspost import SwisspostAddressChecker


class SwisspostAddressCheckerPingTestCase(TestCase):

    @responses.activate
    def test_ping(self):
        responses.add(
            responses.GET,
            'https://webservices.post.ch:17023/IN_SYNSYN_EXT/REST/v1/ping',
            json={},
            status=200
        )
        connector = SwisspostAddressChecker(username='test', password='test')
        response = connector.ping()
        self.assertEqual(response, True)
