from unittest import TestCase

from address_checker.responses import QStatShortReportStatus


class QStatShortReportStatusTestCase(TestCase):

    def test_no_status(self):
        qstat = QStatShortReportStatus(None)

        self.assertEqual(qstat, ['-1', 'No results', 'No results returned by SwissPost\xa0API.'])

    def test_status_not_valid(self):
        qstat = QStatShortReportStatus('3333')

        self.assertEqual(qstat, ['3333', 'No results', 'No results returned by SwissPost\xa0API.'])