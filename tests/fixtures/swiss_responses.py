def query_response(status=0):
    return {
        "RunQuery2Result": {
            "Result": {
                "Data": [
                    {
                        "FieldName": "CustomID_01_in",
                        "Value": ""
                    },
                    {
                        "FieldName": "QSTAT",
                        "Value": "1"
                    },
                    {
                        "FieldName": "ShortReport",
                        "Value": "-11111111111414411111A-A"
                    },
                    {
                        "FieldName": "CorrectionType",
                        "Value": "0"
                    },
                    {
                        "FieldName": "TotalScore",
                        "Value": "100.00"
                    },
                    {
                        "FieldName": "ValidFrom",
                        "Value": ""
                    },
                    {
                        "FieldName": "ValidTo",
                        "Value": ""
                    },
                    {
                        "FieldName": "Birthdate",
                        "Value": ""
                    },
                    {
                        "FieldName": "HouseKey",
                        "Value": "76216853"
                    },
                    {
                        "FieldName": "Double_DoubleGroupNo",
                        "Value": ""
                    },
                    {
                        "FieldName": "Double_DSTAT",
                        "Value": ""
                    },
                    {
                        "FieldName": "Company",
                        "Value": ""
                    },
                    {
                        "FieldName": "Prename",
                        "Value": "Larissa"
                    },
                    {
                        "FieldName": "Prename2",
                        "Value": ""
                    },
                    {
                        "FieldName": "Name",
                        "Value": "Harnisch"
                    },
                    {
                        "FieldName": "MaidenName",
                        "Value": ""
                    },
                    {
                        "FieldName": "AddressAddition",
                        "Value": ""
                    },
                    {
                        "FieldName": "CoAddress",
                        "Value": ""
                    },
                    {
                        "FieldName": "StreetName",
                        "Value": "Bürenstrasse"
                    },
                    {
                        "FieldName": "HouseNo",
                        "Value": "45"
                    },
                    {
                        "FieldName": "HouseNoAddition",
                        "Value": ""
                    },
                    {
                        "FieldName": "Floor",
                        "Value": ""
                    },
                    {
                        "FieldName": "ZIPCode",
                        "Value": "2504"
                    },
                    {
                        "FieldName": "ZIPAddition",
                        "Value": "00"
                    },
                    {
                        "FieldName": "TownName",
                        "Value": "Biel/Bienne"
                    },
                    {
                        "FieldName": "Canton",
                        "Value": "BE"
                    },
                    {
                        "FieldName": "CountryCode",
                        "Value": "CH"
                    },
                    {
                        "FieldName": "POBoxTerm",
                        "Value": ""
                    },
                    {
                        "FieldName": "POBoxNo",
                        "Value": ""
                    },
                    {
                        "FieldName": "POBoxZIP",
                        "Value": ""
                    },
                    {
                        "FieldName": "POBoxZIPAddition",
                        "Value": ""
                    },
                    {
                        "FieldName": "POBoxTownName",
                        "Value": ""
                    }
                ],
                "Id": 312312312
            },
            "SettlementId": 872693,
            "Status": status
        }
    }


def autocomplete_response(status=0):
    return {
        'QueryAutoComplete2Result': {
            'AutoCompleteResult': [],
            'Status': status
        }
    }


def verification_response():
    return {
        "QueryBuildingVerificationResult": {
            "BuildingVerificationData": {
                "HouseKey": 7013859,
                "HouseNumber": "21",
                "HouseNumberAddition": "",
                "HouseNumberHouseNumberAddition": None,
                "Pstat": 4,
                "Streetname": "Viktoriastrasse",
                "StreetnameLong": "Viktoriastrasse",
                "TownName": "Bern",
                "TownNameLong": "Bern",
                "ZipAddition": "00",
                "Zipcode": "3013"
            },
            "Status": 0
        }
    }
