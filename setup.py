# -*- coding: utf-8 -*-
import os
from setuptools import setup

from address_checker import __version__


version = __version__
readme = open('README.md').read()
history = open('CHANGELOG.md').read()


with open('requirements/base.txt') as fp:
    install_requires = fp.read().splitlines()
with open('requirements/tests.txt') as fp:
    test_install_requires = fp.read().splitlines()[1:]


def get_packages(package):
    return [
        dirpath for dirpath, _, _ in os.walk(package)
        if os.path.exists(os.path.join(dirpath, '__init__.py'))
    ]


setup(
    name='address_checker',
    version=__version__,
    packages=get_packages('address_checker'),
    license='License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    description='Python client to the Swiss Post Address Checking service',
    long_description=readme + '\n' + history,
    install_requires=install_requires,
    url='https://gitlab.com/twistlabdev/swisspost_addresschecker',
    author='Twist Lab',
    author_email='dev@twistlab.ch',
    scripts=['bin/address-check-shell.py'],
    test_suite='tests',
    tests_require=test_install_requires,
    classifiers=[
        'Development Status :: Beta',
        'Intended Audience :: Developers',
        'License :: No License',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries',
    ]
)
