# Twist Lab - Swiss Post Address Checker

This project provides a Python client to the [Swiss Post Addresschecker  service](https://www.poste.ch/traitement-adresses).

Technical doc: https://developer.post.ch/en/address-web-services-rest

## API Endpoints implemented

| Feature      | Endpoint | Documentation section     |
| :---        |    :---   |          :--- |
| Individual address query      | POST /runquery2       | 4.3   |
| Company address query   | POST /runquery2        | 4.3      |
| Autocomplete   | POST /autocomplete2        | 4.4      |
| Building verification   | GET /simplequery?params        | 4.5      |

## Usage

Basic usage connecting and running a ping request:

    from address_checker.swisspost import SwisspostAddressChecker

    addresschecker = SwisspostAddressChecker(username, password)
    addresschecker.ping()

Example usage requesting individual_address_query endpoint:


```python
response = addresschecker.individual_address_query(
    first_name=first_name,
    last_name=last_name,
    street_name=street,
    house_number=house_number,
    zip_code=zip_code,
    city=town
)
print(response.result)
```

Example usage of company address query

```python
response = addresschecker.company_address_query(
    company_name=company_name,
    street_name=street,
    house_number=house_number,
    zip_code=zip_code,
    city=town
)
print(response.result)
```

Example usage requesting autocomplete endpoint:

```python
response = addresschecker.autocomplete(
    street_name=street,
    house_number=house_number,
    zip_code=zip_code,
    city=town
)
print(response.result)
```

## CLI
A command line interpreter is available for testing and debugging purposes where the response is parsed and the main result fields are presented as a table for an easy interpretation.

![Example of parsed data](./docs/images/example_parsedata.png)


## Dependencies
- requests 2.23.0
- tabulate 0.8.7 (for CLI)

## Running tests

Create a virtual env:

    python3 -m venv ~/.venvs/venv_swisspost_adresschecker

Activate it:

    source ~/.venvs/venv_swisspost_adresschecker/bin/activate

Install tox:

    pip install tox

To run all test simply run:

    tox

Tests can also be run in your current env without tox:
    
    pip install -r requirements/tests.txt
    python -m pytest


### Code coverage

The coverage is automatically reported within tox. To run it manually:
    
    pip install -r requirements/tests.txt
    pip install coverage
    coverage run -m pytest
    coverage report

Html report can be obtained with (it will be stored in `htmlcov` folder):

    coverage html

Alternatively you can use tox with:

    tox -e coverage

### Python version

Compatible versions:
* Python 3.6
* Python 3.7
* Python 3.8

---
(c) Twist Lab Sàrl | www.twistlab.ch

