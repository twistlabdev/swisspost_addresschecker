# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.6] - 2020-12-22
- Accept house number addition without house number (such as in Zentrum C)
- Fix an issue where CLI would not handle SWISSPOST_USE_DEV env var

## [1.0.5] - 2020-12-09
- Separate house number addition from house number (e.g. route de Berne 20ter will be treated as House no "20", 
house no addition: "ter")

# [1.0.4] - 2020-09-04
- Fix small issue in setup.py, raising a warning in pip

## [1.0.3] - 2020-06-29
- Use production server by default in shell script (change via SWISSPOST_USE_DEV env var)

## [1.0.2] - 2020-06-05
- Intercept network errors (timeout, connection error)
- Intercept Swiss Post server errors (500)

## [1.0.1] - 2020-05-20
- Adapted response codes to latest doc
- 100% test coverage

## [1.0.0] - 2020-05-09

Implemented new endpoints. 

| Feature      | Endpoint | Documentation section     |
| :---        |    :---   |          :--- |
| Individual address query      | POST /runquery2       | 4.3   |
| Company address query   | POST /runquery2        | 4.3      |
| Autocomplete   | POST /autocomplete2        | 4.4      |
| Building verification   | GET /simplequery?params        | 4.5      |

Improved CLI to parse result codes and display the information as a fancy table.

## [0.1.0] - 2020-04-27

Initial commit, first working version. Endpoints implemented:

| Feature      | Endpoint | Documentation section     |
| :---        |    :---   |          :--- |
| Individual address query      | POST /runquery2       | 4.3   |
