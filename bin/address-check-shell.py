#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import getpass
import os

from pprint import pprint
from tabulate import tabulate

from address_checker.config import SWISSPOST_TEST_URL
from address_checker.swisspost import SwisspostAddressChecker, SWISSPOST_PRODUCTION_URL


def show_protocol(response):
    print("----------------------------------------------------")
    if input("Show protocol? [y/N] ").lower() == 'y':
        pprint(response.printable_protocol)


def print_response(response):
    print("----------------------------------------------------")
    print("Response:")
    pprint(response.result)
    print("----------------------------------------------------")

    print("----------------------------------------------------")
    print("Parsed QSTAT result:")
    print(tabulate([response.result_hit], headers=["Code", "Definition", "Description"], tablefmt="fancy_grid"))
    print("Parsed ShortReport result:")
    print(tabulate(response.result_short_report, headers=["Field", "Return code", "Definition", "Description"],
                   tablefmt="fancy_grid"))
    show_protocol(response)


def handle_exception(exception):
    print("----------------------------------------------------")
    print("An error occured:")
    print(" " + str(exception))
    print("----------------------------------------------------")


user_input = None

print("----------------------------------------------------")
print("| TWISTLAB - Swiss Post Address Checker            |")
print("----------------------------------------------------")

# SECURITY DETAILS #################################################
try:
    username = os.environ['SWISSPOST_USERNAME']
except KeyError:
    username = input('Technical User ID (TU) [i.e. TU_XXXXXXX_XXXX]: ')

try:
    password = os.environ['SWISSPOST_PASSWORD']
except KeyError:
    password = getpass.getpass('Technical User password: ')

try:
    use_dev = int(os.environ['SWISSPOST_USE_DEV']) != 0
except KeyError:
    use_dev = False
except ValueError:
    use_dev = False

if use_dev:
    service_url = SWISSPOST_TEST_URL
else:
    service_url = SWISSPOST_PRODUCTION_URL
####################################################################
addresschecker = SwisspostAddressChecker(username, password, service_url=service_url)
addresschecker.ping()

while user_input != "q" and user_input != 'Q':
    print("""--- MENU ---
[1] Check an individual address    
[2] Check a company address
[3] Autocomplete an address
[4] Building verification
[q] Quit""")
    user_input = input() 

    if user_input == '1' or user_input == "":
        # [1] Check an individual address
        first_name = input("First name [i.e: 'Larissa']: ")
        last_name = input("Last name [i.e: 'Harnisch']: ")
        street = input("Street [i.e: 'Bürenstrasse']: ")
        house_number = input("House number [i.e: '45']: ")
        zip_code = input("Zip [i.e: '2504']: ")
        town = input("Town [i.e: 'Biel/Bienne']: ")
        try:
            res = addresschecker.individual_address_query(
                first_name=first_name,
                last_name=last_name,
                street_name=street,
                house_number=house_number,
                zip_code=zip_code,
                city=town
            )
            print_response(res)
        except Exception as e:
            handle_exception(e)

    elif user_input == '2':
        # [2] Check a company address
        company_name = input("Company name [i.e: 'Bazar']: ")
        street = input("Street [i.e: 'Brüggstrasse']: ")
        house_number = input("House number [i.e: '6']: ")
        zip_code = input("Zip [i.e: '2503']: ")
        town = input("Town [i.e: 'Biel/Bienne']: ")
        try:
            res = addresschecker.company_address_query(
                company_name=company_name,
                street_name=street,
                house_number=house_number,
                zip_code=zip_code,
                city=town
            )
            print_response(res)
        except Exception as e:
            handle_exception(e)

    elif user_input == '3':
        # [3] Autocomplete an address
        street = input("Street [i.e: 'Viktoriastrasse']: ")
        house_number = input("House number [i.e: '21']: ")
        zip_code = input("Zip [i.e: '3013']: ")
        town = input("Town [i.e: 'Berne']: ")
        try:
            res = addresschecker.autocomplete(
                street_name=street,
                house_number=house_number,
                zip_code=zip_code,
                city=town
            )
            print("----------------------------------------------------")
            print("JSON Response:")
            pprint(res.result)
            print("----------------------------------------------------")
            print("Tabular view:")
            print(
                tabulate(
                    [(item.get('Zipcode'), item.get('TownName'), item.get('Streetname'),
                     item.get('HouseNumber')) for item in res.result],
                    headers=["ZipCode", "TownName", "Streetname", "HouseNumber"],
                    tablefmt="fancy_grid"
                )
            )
            show_protocol(res)

        except Exception as e:
            handle_exception(e)

    elif user_input == '4':
        # [4] Building verification
        street = input("Street [i.e: 'Viktoriastrasse']: ")
        house_number = input("House number [i.e: '21']: ")
        house_number_additional = input("House additional number [i.e: '21']: ")
        zip_code = input("Zip [i.e: '3013']: ")
        town = input("Town [i.e: 'Berne']: ")
        try:
            res = addresschecker.building_verification(
                street_name=street,
                house_number=house_number,
                house_number_additional=house_number_additional,
                zip_code=zip_code,
                city=town
            )

            print("----------------------------------------------------")
            print("Response:")
            pprint(res.result)
            print("----------------------------------------------------")

            print("----------------------------------------------------")
            print("Parsed PSTAT result:")
            print(
                tabulate(
                    [res.p_stat.result_report],
                    headers=["Code", "Definition", "Description"], tablefmt="fancy_grid"
                )
            )
            show_protocol(res)
        except Exception as e:
            handle_exception(e)
    else:
        exit()
