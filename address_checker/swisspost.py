# -*- coding: utf-8 -*-
from datetime import datetime
import json
import random
import re
import requests

from . import exceptions
from .config import DEFAULT_TIMEOUT, SWISSPOST_PRODUCTION_URL
from .responses import (
    SwisspostAddressAutocompleteResponse, SwisspostAddressBuildingVerificationResponse,
    SwisspostAddressCheckResponse
)

ADDRESS_VALIDATION_SERVICE = "Eirene_Validation_v1"
ADDRESS_MAINTENANCE_SERVICE = "Eireine_maintenance_v1"

ADDRESS_QUERY_ENDPOINT = 'runquery2'
AUTOCOMPLETE_ENDPOINT = 'autocomplete2'
BUILDING_VERIFICATION_ENDPOINT = 'simplequery/buildingverification'
PING_ENDPOINT = 'ping'


class SwisspostAddressChecker:
    def __init__(self, username=None, password=None, service_url=SWISSPOST_PRODUCTION_URL, timeout=DEFAULT_TIMEOUT,
                 service_level=ADDRESS_VALIDATION_SERVICE, proxy_url=None, proxy_port=None):
        self.url = service_url
        self.proxies = None
        self.auth = None
        self.timeout = timeout
        self.current_protocol = []
        self.service_level = service_level

        if username:
            self.auth = (username, password)

        if proxy_url and proxy_port:
            self.proxies = {
                'http': 'http://{}:{}'.format(proxy_url, proxy_port),
                'https': 'https://{}:{}'.format(proxy_url, proxy_port),
            }

    @staticmethod
    def _field_struct(field, value):
        return {
            'FieldName': field,
            'Value': value
        }

    def _get(self, method, params=None):
        url = '{}/{}'.format(self.url, method)
        try:
            response = requests.get(url, params=params, auth=self.auth, timeout=self.timeout, proxies=self.proxies)
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise exceptions.SwissPostServerError() from err
        except requests.exceptions.RequestException as err:
            raise exceptions.SwissPostNetworkError() from err
        return response

    def _post(self, method, data=None):
        url = '{}/{}'.format(self.url, method)
        try:
            response = requests.post(url, json=data, auth=self.auth, timeout=self.timeout, proxies=self.proxies)
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise exceptions.SwissPostServerError() from err
        except requests.exceptions.RequestException as err:
            raise exceptions.SwissPostNetworkError() from err
        return response

    def _protocol(self, level, message, timestamp):
        """
        Internal protocoling method.

        Keyword arguments:
        level -- The message level (INFO / TITLE)
        message -- The message to protocol
        timestamp -- True/False: record timestamp or not
        """
        if timestamp:
            self.current_protocol.append((datetime.now(), level, message))
        else:
            self.current_protocol.append((None, level, message))

    def _protocol_heading(self):
        self._protocol("TITLE", "BEGINING OF ADDRESS CHECKING PROTOCOL", False)
        self._protocol("INFO", "Protocoling started", True)
        self._protocol("TITLE", "Inputs:", False)

    def _protocol_request_individual_query(self, payload):
        self._protocol("TITLE", "Raw individual_address_query data (json)", False)
        self._protocol("INFO", json.dumps(payload, indent=4), False)
        self._protocol("INFO", "Calling: " + str(self.url), True)

    def _protocol_response(self, response, reset_current_protocol=True):
        self._protocol("INFO", "Response received", True)
        self._protocol("TITLE", "Raw reponse data (json)", False)
        self._protocol("INFO", json.dumps(response.json(), indent=4), False)
        self._protocol("INFO", "Protocoling ended", True)
        self._protocol("TITLE", "END OF PROTOCOL", False)
        query_protocol = self.current_protocol
        if reset_current_protocol:
            self.current_protocol = []
        return query_protocol

    @staticmethod
    def _split_house_nb(house_number):
        parts = re.findall(r'[A-Za-z]+|\d+', house_number)
        if len(parts) == 2:
            return parts
        if not house_number.isdigit():
            return None, house_number
        return house_number, None

    def individual_address_query(self, first_name, last_name, street_name, house_number, zip_code, city,
                                 external_id=None):
        """
        Check an address validity for individual.

        Check Swiss Post docs section 4.3:
            https://developer.post.ch/en/address-web-services-rest

        Keyword arguments:
        first_name -- First name to check for (i.e. Felix)
        last_name -- Last name to check for (i.e. Model)
        street_name -- Street address (i.e. Viktoriastrasse)
        house_number -- Street house number (i.e. 21)
        zip_code -- Zip code (i.e. 3013)
        city -- Town name (i.e. Berne)
        external_id -- Id for request for traceability reason (random by default).
        """
        if not external_id:
            external_id = random.randint(0, 100000)
        self._protocol_heading()
        self._protocol("INFO", "SearchType: 1", False)
        self._protocol("INFO", "First name: " + first_name, False)
        self._protocol("INFO", "Last name: " + last_name, False)
        self._protocol("INFO", "Street: " + street_name, False)
        self._protocol("INFO", "House Number: " + house_number, False)
        self._protocol("INFO", "Zip Code: " + zip_code, False)
        self._protocol("INFO", "Town: " + city, False)

        house_number, additional_house_number = self._split_house_nb(house_number)
        payload = {
            'address': {
                'Id': external_id,
                'Data': [
                    self._field_struct('Prename_in', first_name),
                    self._field_struct('Name_in', last_name),
                    self._field_struct('StreetName_in', street_name),
                    self._field_struct('ZIPCode_in', zip_code),
                    self._field_struct('TownName_in', city),
                ]
            },
            'key': self.service_level,
            'timeout': int(self.timeout * 1000)
        }
        if house_number:
            payload['address']['Data'].append(
                self._field_struct('HouseNo_in', house_number)
            )
        if additional_house_number:
            payload['address']['Data'].append(
                self._field_struct('HouseNoAddition_in', additional_house_number)
            )

        self._protocol_request_individual_query(payload)
        response = self._post(ADDRESS_QUERY_ENDPOINT, data=payload)
        query_protocol = self._protocol_response(response)
        return SwisspostAddressCheckResponse(
            request=payload,
            response=response,
            protocol=query_protocol
        )

    def company_address_query(self, company_name, street_name, house_number, zip_code, city, external_id=None):
        """
        Check an address validity for companies.

        Check Swiss Post docs section 4.3:
            https://developer.post.ch/en/address-web-services-rest

        Keyword arguments:
        company_name -- Company name (i.e. Bazar)
        street_name -- Street address (i.e. Viktoriastrasse)
        house_number -- Street house number (i.e. 21)
        zip_code -- Zip code (i.e. 3013)
        city -- Town name (i.e. Berne)
        external_id -- Id for request for traceability reason (random by default).
        """
        if not external_id:
            external_id = random.randint(0, 100000)
        self._protocol_heading()
        self._protocol("INFO", "SearchType: 1", False)
        self._protocol("INFO", "Company name: " + company_name, False)
        self._protocol("INFO", "Street: " + street_name, False)
        self._protocol("INFO", "House Number: " + house_number, False)
        self._protocol("INFO", "Zip Code: " + zip_code, False)
        self._protocol("INFO", "Town: " + city, False)

        house_number, additional_house_number = self._split_house_nb(house_number)
        payload = {
            'address': {
                'Id': external_id,
                'Data': [
                    self._field_struct('Company_in', company_name),
                    self._field_struct('StreetName_in', street_name),
                    self._field_struct('ZIPCode_in', zip_code),
                    self._field_struct('TownName_in', city),
                ]
            },
            'key': self.service_level,
            'timeout': int(self.timeout * 1000)
        }
        if house_number:
            payload['address']['Data'].append(
                self._field_struct('HouseNo_in', house_number)
            )
        if additional_house_number:
            payload['address']['Data'].append(
                self._field_struct('HouseNoAddition_in', additional_house_number)
            )
        self._protocol_request_individual_query(payload)
        response = self._post(ADDRESS_QUERY_ENDPOINT, data=payload)
        query_protocol = self._protocol_response(response)
        return SwisspostAddressCheckResponse(
            request=payload,
            response=response,
            protocol=query_protocol
        )

    def autocomplete(self, street_name, house_number, zip_code, city,
                     onrp=0, str_id=0, house_key=0,
                     zip_addition='', house_number_addition='', zip_order_mode=0):
        """
        Query for auto complete address.

        There are different queries depending on param for searching
        building and streets. Check Swiss Post docs section 4.4:
            https://developer.post.ch/en/address-web-services-rest

        Keyword arguments:
        street_name -- Street address (i.e. Viktoriastrasse)
        house_number -- Street house number (i.e. 21)
        zip_code -- Zip code (i.e. 3013)
        city -- Town name (i.e. Berne)
        """
        self._protocol_heading()
        self._protocol("INFO", "Zip Code: " + zip_code, False)
        self._protocol("INFO", "Town: " + city, False)

        payload = {
            'request': {
                'Zipcode': zip_code,
                'TownName': city,
                'Streetname': street_name,
                'HouseNumber': house_number,
                'Onrp': onrp,
                'StrId': str_id,
                'HouseKey': house_key,
                'ZipAddition': zip_addition,
                'HouseNumberAddition': house_number_addition
            },
            'zipOrderMode': zip_order_mode
        }
        self._protocol_request_individual_query(payload)
        response = self._post(AUTOCOMPLETE_ENDPOINT, data=payload)
        self._protocol_response(response, False)
        return SwisspostAddressAutocompleteResponse(
            request=payload,
            response=response,
            current_protocol=self.current_protocol
        )

    def building_verification(self, street_name, house_number, house_number_additional, zip_code, city):
        """
        Query for building verification.
        With BuildingVerification, a non-personalized address can be
        checked for accuracy. If the address is accurate, the house key
        and the address are entered with the Swiss Post spelling.
        Check Swiss Post docs section 4.5:
            https://developer.post.ch/en/address-web-services-rest
        Keyword arguments:
        street_name -- Street address (i.e. Viktoriastrasse)
        house_number -- Street house number (i.e. 21)
        house_number_additional -- Additional house number information
        zip_code -- Zip code (i.e. 3013)
        city -- Town name (i.e. Berne)
        """
        self._protocol_heading()
        self._protocol("INFO", "Zip Code: " + zip_code, False)
        self._protocol("INFO", "Town: " + city, False)

        payload = {
            'streetname': street_name,
            'houseNo': house_number,
            'houseNoAddition': house_number_additional,
            'zipcode': zip_code,
            'townname': city,
        }
        self._protocol_request_individual_query(payload)

        response = self._get(BUILDING_VERIFICATION_ENDPOINT, params=payload)
        self._protocol_response(response, False)
        return SwisspostAddressBuildingVerificationResponse(
            request=payload,
            response=response,
            current_protocol=self.current_protocol
        )

    def ping(self):
        """
        Check if service is available.

        The easiest method for checking whether the SWISS POST API
        service is available.

        Check Swiss Post docs section 4.1:
            https://developer.post.ch/en/address-web-services-rest.
        """
        response = self._get(PING_ENDPOINT)
        return response.status_code == 200
