from address_checker.exceptions import EXCEPTION_STATUS_CODE_MAPPER


class QueryResponseValidator:
    def __init__(self, response_data):
        self.response_data = response_data

    def validate(self):
        status = self.response_data['RunQuery2Result']['Status']

        if status != 0:
            exception = EXCEPTION_STATUS_CODE_MAPPER[status]
            raise exception(response_data=self.response_data)

        return self.response_data


class AutocompleteResponseValidator:
    def __init__(self, response_data):
        self.response_data = response_data

    def validate(self):
        status = self.response_data['QueryAutoComplete2Result']['Status']

        if status != 0:
            exception = EXCEPTION_STATUS_CODE_MAPPER[status]
            raise exception(response_data=self.response_data)

        return self.response_data
