from copy import deepcopy

from .exceptions import InvalidShortReport
from .validators import AutocompleteResponseValidator, QueryResponseValidator


class SwisspostResponse:
    protocol = []

    @property
    def printable_protocol(self):
        """
        Return the protocol as a print friendly string
        """
        protocol_to_return = ""
        for protocolentry in self.protocol:
            date_and_time = "" if not protocolentry[0] else "[" + str(protocolentry[0]) + "] "
            if protocolentry[1] == "INFO":
                protocol_to_return += date_and_time + str(protocolentry[2]) + "\n"
            else:
                protocol_to_return += "****************************************************\n"
                protocol_to_return += date_and_time + str(protocolentry[2]) + "\n"
                protocol_to_return += "****************************************************\n"
        return protocol_to_return


class SwisspostAddressCheckResponse(SwisspostResponse):
    """Response for RunQuery2"""
    qstat = None
    result = dict()
    result_hit = None
    result_short_report = None
    correct_type_detail = ''

    def __init__(self, request, response, protocol):
        QueryResponseValidator(response_data=response.json()).validate()
        self.request = request
        self.response = response
        self.protocol = protocol
        self._process_result(response)

    def _process_result(self, response):
        result_json = response.json()['RunQuery2Result']['Result']
        self.result = deepcopy(result_json)

        parsed_data = {key.get('FieldName'): key.get('Value') for key in result_json['Data']}
        self.result['Data'] = parsed_data
        if parsed_data.get('CorrectionType') == '0':
            self.correct_type_detail = 'No correction required'

        elif parsed_data.get('CorrectionType') == '1':
            self.correct_type_detail = 'Correction required'

        self.result['Data']['CorrectionType'] = {
            'Code': parsed_data.get('CorrectionType'),
            'Definition': self.correct_type_detail
        }
        self._process_result_hits(parsed_data)

    def _process_result_hits(self, parsed_data):
        # add further interpretation to main query result code (QSTAT)
        self.qstat = QStat(parsed_data.get('QSTAT'))
        self.result['Data']['QSTAT'] = {
            'Code': self.qstat.code,
            'Definition': self.qstat.designation,
            'Description': self.qstat.description
        }

        # following returns arrays for tabular print (only for debugging purposes)
        self.result_hit = self.qstat.result_report
        self.result_short_report = QStatShortReport(parsed_data.get('ShortReport'))


class SwisspostAddressBuildingVerificationResponse(SwisspostResponse):
    """Response for BuildingVerification"""

    def __init__(self, request, response, current_protocol):
        self.request = request
        self.response = response
        self.protocol = current_protocol
        self.content = response.content
        self.result = response.json()['QueryBuildingVerificationResult']['BuildingVerificationData']

        self.p_stat = PStat(self.result['Pstat'])
        self.result['Pstat'] = {
            'Code': self.p_stat.code,
            'Definition': self.p_stat.designation,
            'Description': self.p_stat.description
        }


class SwisspostAddressAutocompleteResponse(SwisspostResponse):
    """Response for QueryAutoComplete2"""

    def __init__(self, request, response, current_protocol):
        AutocompleteResponseValidator(response_data=response.json()).validate()
        self.request = request
        self.response = response
        self.protocol = current_protocol
        self.content = response.content
        self.result = response.json()['QueryAutoComplete2Result']['AutoCompleteResult']


class QStat:
    def __init__(self, qstat):
        self.code = '-1'
        self.designation = 'Non-deliverable'
        self.description = 'Non-deliverable or Delivery not guaranteed.'

        if qstat:
            self.code = qstat
            parse_code = self.DESIGNATIONS_DESCRIPTIONS.get(str(qstat))
            if parse_code:
                self.designation = parse_code[0]
                self.description = parse_code[1]

    @property
    def result_report(self):
        return [self.code, self.designation, self.description]

    def __str__(self):
        return 'QSAT CODE: {} - DESIGNATION: {} - DESCRIPTION: {}'.format(self.code, self.designation, self.description)

    DESIGNATIONS_DESCRIPTIONS = {
        '1': (
            'Person hit',
            'This person (first names and last name) is known at this address and items can be delivered.'
        ),
        '2': (
            'Household hit',
            'This household or last name (regardless of the  first name) is known at this address and items can be '
            'delivered.'
        ),
        '3': (
            'Company hit',
            'This company is known at this address and items can be delivered.'
        ),
        '4': (
            'Relocation hit',
            'A movers’ address is available for this person or company.'
        ),
        '25': (
            'Deceased / Company no longer in business ',
            'This person is deceased or this company is no longer in business.'
        ),
        '26': (
            'International relocation hit',
            'An unverified international address is available for this person or company.'
        ),
        '27': (
            'Relocated, unknown',
            'This person or company is no longer located at this address. The new address is unknown or is not '
            'permitted to be disclosed.'
        ),
        '50': (
            'Person unknown at this address',
            'The person, company or household is unknown at this address.'
        ),
        '51': (
            'Address unknown',
            'This address is incorrect or incomplete.'
        ),
        'K': (
            'Original',
            'This address is contained in the address database several times and corresponds to a high degree with '
            'the other data records of the duplicate group.'
        ),
        'F': (
            'Duplicate',
            'This address is contained in the address database several times and does not correspond as much with '
            'the other data records of the duplicate group.'
        ),
    }


class QStatShortReport:  # pylint: disable=too-many-instance-attributes

    def __new__(cls, qstat_short_report):
        instance = super(QStatShortReport, cls).__new__(cls)
        instance.__init__(qstat_short_report)

        status_data = []
        for field in dir(instance):
            if not field.startswith('__') and field != 'code':
                attribute = getattr(instance, field)
                attribute.insert(0, field)
                status_data.append(attribute)
        return status_data

    def __init__(self, qstat_short_report):
        self.code = ''

        if qstat_short_report:

            if len(qstat_short_report) != 24:
                raise InvalidShortReport('Invalid short report {}'.format(qstat_short_report))

            self.code = qstat_short_report

            self.Company = QStatShortReportStatus(self.code[0])
            self.Prename = QStatShortReportStatus(self.code[1])
            self.Prename2 = QStatShortReportStatus(self.code[2])
            self.Name = QStatShortReportStatus(self.code[3])
            self.MaidenName = QStatShortReportStatus(self.code[4])
            self.AddressAddition = QStatShortReportStatus(self.code[5])
            self.CoAddress = QStatShortReportStatus(self.code[6])
            self.StreetName = QStatShortReportStatus(self.code[7])
            self.HouseNo = QStatShortReportStatus(self.code[8])
            self.HouseNoAddition = QStatShortReportStatus(self.code[9])
            self.Floor = QStatShortReportStatus(self.code[10])
            self.ZIPCode = QStatShortReportStatus(self.code[11])
            self.ZIPAddition = QStatShortReportStatus(self.code[12])
            self.TownName = QStatShortReportStatus(self.code[13])
            self.Canton = QStatShortReportStatus(self.code[14])
            self.CountryCode = QStatShortReportStatus(self.code[15])
            self.PoBoxTerm = QStatShortReportStatus(self.code[16])
            self.PoBoxNo = QStatShortReportStatus(self.code[17])
            self.PoBoxZIP = QStatShortReportStatus(self.code[18])
            self.PoBoxZIPAddition = QStatShortReportStatus(self.code[19])
            self.PoBoxTownName = QStatShortReportStatus(self.code[20])
            self.address_type = QStatShortReportStatus(self.code[21])
            self.source_type = QStatShortReportStatus(self.code[22])
            self.address_status = QStatShortReportStatus(self.code[23])


class QStatShortReportStatus:

    def __new__(cls, short_report_code):
        instance = super(QStatShortReportStatus, cls).__new__(cls)
        instance.__init__(short_report_code)
        return [instance.code, instance.description, instance.detailed_description]

    def __init__(self, short_report_code):
        self.code = '-1'
        self.description = 'No results'
        self.detailed_description = 'No results returned by SwissPost API.'

        if short_report_code:
            self.code = short_report_code

            parse_code = self.DESCRIPTIONS_DETAIL.get(str(short_report_code))
            if parse_code:
                self.description = parse_code[0]
                self.detailed_description = parse_code[1]

            elif str(short_report_code).isalpha():
                self.description = 'Special'
                self.detailed_description = ('Definition according to the field '
                                             '(e.g. address type) or special case for a specific product.')

    DESCRIPTIONS_DETAIL = {
        '-': (
            'No statement ',
            'Input content not analysed.'
        ),
        '1': (
            'Compliance',
            'Input content checked: OK, complete compliance.'
        ),
        '2': (
            'Hit with discrepancy ',
            'Input content checked: OK, complete compliance.'
        ),
        '3': (
            'Changed',
            'Input content checked: correction of address elements or address elements\n'
            'identified as archive input and replaced by official designation in output.'
        ),
        '4': (
            'Supplemented',
            'Input content checked: missing information could be completed (additional\n'
            'house number information, canton abbreviation, etc.).'
        ),
        '5': (
            'New information',
            'Input content checked: information concerning a move has been found for the\n'
            'address checked.'
        ),
        '6': (
            'Blocked forwarding information',
            'Input content checked: blocked information concerning a move exists for the\n'
            'address checked. Post CH Ltd is not authorized to forward the new address.'
        ),
        '9': (
            'Deleted',
            'Input content checked: the information concerning the address checked is no\n'
            'longer valid and has been deleted.'
        ),
        '0': (
            'No hit',
            'Input content checked: no suitable information has been found in the Post CH Ltd\n'
            'reference data concerning the address checked.'
        ),
    }


class PStat:

    def __init__(self, pstat):

        self.code = '-1'
        self.designation = 'No results'
        self.description = 'No results returned by SwissPost API.'

        if pstat:
            self.code = pstat

            parse_code = self.DESIGNATIONS_DESCRIPTIONS.get(str(pstat))
            if parse_code:
                self.designation = parse_code[0]
                self.description = parse_code[1]

    @property
    def result_report(self):
        return [self.code, self.designation, self.description]

    def __str__(self):
        return '{} - {}: {}'.format(self.code, self.designation, self.description)

    DESIGNATIONS_DESCRIPTIONS = {
        '1': (
            'ok',
            ("The address is correct for 'Street name', 'House number',"
             " 'Postcode' and 'Location'.")
        ),
        '2': (
            'standardised',
            ("The address could be normalised according to Post CH Ltd.'s"
             " guidelines for addresses.")
        ),
        '3': (
            'changed',
            "Spelling mistakes in the address could be corrected."
        ),
        '4': (
            'supplemented',
            "Information missing from the address can be supplemented."
        ),
        '5': (
            'archive input',
            "The entry contained addresses with alternative terms."
        ),
        '6': (
            'Partial supplement',
            ("The address could not be standardized/corrected according "
             "to Swiss Post guidelines in sub-areas. The postal address "
             "is not yet correct, however.")
        ),
        '7': (
            'invalid',
            ("Capture errors or Street is not included in the "
             "reference data")
        ),
        '8': (
            'ambiguous',
            ("As a result several addresses will come into question. The"
             " input dataset will be applied to the output file.")
        ),
        '9': (
            'unchecked',
            ("The requested entered country is not listed in «postal "
             "validation» and therefore cannot be checked.")
        ),
        '10': (
            'Error',
            (
                "Due to an error (e.g. postcode in name of city, empty "
                "input fields, house-number only given for street, "
                "house-number comprises alphabetical characters only, "
                "greater than 5 characters, therefore it was not possible "
                "to produce a result.")
        ),
    }
