
class SwissPostException(Exception):
    def __init__(self, *args, **kwargs):
        if 'response_data' in kwargs:
            self.response_data = kwargs.pop('response_data')
        else:
            self.response_data = None
        super().__init__(*args, **kwargs)


class SwissPostNetworkError(SwissPostException):
    pass


class SwissPostServerError(SwissPostException):
    pass


class AuthenticationFailed(SwissPostException):
    """Authentication failed."""
    code = '101'


class AuthenticationUserUnknown(SwissPostException):
    """Unknown user."""
    code = '102'


class AuthenticationUserMissingRole(SwissPostException):
    """The key entered is unknown."""
    code = '103'


class AuthenticationUserNotOwner(SwissPostException):
    """The request fidoes not belong to the user indicated during the individual_address_query."""
    code = '104'


class InternalServerCallFailed(SwissPostException):
    """An internal error has occurred in the service. """
    code = '201'


class InternalServerCall(SwissPostException):
    """An internal error has occurred in the service."""
    code = '202'


class InternalServerCallTimeout(SwissPostException):
    """The individual_address_query lasted longer than the defined timeout."""
    code = '203'


class DatabaseOperationFailed(SwissPostException):
    """An error occurred when accessing the database."""
    code = '301'


class DatabaseItemNotFound(SwissPostException):
    """An error occurred when accessing the database."""
    code = '302'


class SettlementCreateFailed(SwissPostException):
    """An error occurred when recording the number for calculation purposes."""
    code = '401'


class TokenParameterInvalidFormatException(SwissPostException):
    """A parameter that has been entered does not have the correct format."""
    code = '801'


class InvalidGeoCoordinateFormat(SwissPostException):
    """A parameter that has been entered does not have the correct format."""
    code = '802'


class UnknownError(SwissPostException):
    """Unknown error."""
    code = '999'


EXCEPTION_STATUS_CODE_MAPPER = {
    101: AuthenticationFailed,
    102: AuthenticationUserUnknown,
    103: AuthenticationUserMissingRole,
    104: AuthenticationUserNotOwner,
    201: InternalServerCallFailed,
    202: InternalServerCall,
    203: InternalServerCallTimeout,
    301: DatabaseOperationFailed,
    302: DatabaseItemNotFound,
    401: SettlementCreateFailed,
    801: TokenParameterInvalidFormatException,
    802: InvalidGeoCoordinateFormat,
    999: UnknownError,
}


class InvalidShortReport(Exception):
    """Raised when short report in response is invalid."""
